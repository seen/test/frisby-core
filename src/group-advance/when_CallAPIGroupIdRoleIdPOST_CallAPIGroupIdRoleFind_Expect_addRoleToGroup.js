/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
const frisby = require('frisby');
const Joi = frisby.Joi;
const config = require('../config.js');
const qs = require('qs');

// Do setup first
frisby.globalSetup({
    request: {
        headers: {
            'Authorization': 'Basic ' + Buffer.from(config.admin.login + ":" + config.admin.password).toString('base64'),
            'Content-Type': 'application/x-www-form-urlencoded'
        }, timeout: (20000)
    }
});


it('add role to group after get role of group', function (done) {
    var groupName = 'testGroup_' + config.randomString();
    var roleName = 'testRole_' + config.randomString();
    frisby
    /*
     * Setup Test
     * */
        .post(config.endpoint + '/api/group/new', {
            body: qs.stringify({
                'name': groupName,
                'description': 'when_CallAPIGroupIdRoleIdPOST_CallAPIGroupIdRoleFind_Expect_addRoleToGroup'
            })
        })
        .expect('status', 200)
        .then(function (res) {
            var groupId = res._body.id;
            frisby
                .post(config.endpoint + '/api/role/new', {
                    body: qs.stringify({
                        'name': roleName,
                        'description': 'when_CallAPIGroupIdRoleIdPOST_CallAPIGroupIdRoleFind_Expect_addRoleToGroup',
                        'application': 'testApp',
                        'code_name': 'test_' + config.randomString()
                    })
                })
                .then(function (res1) {
                    var roleId = res1._body.id;

                    /*
                     * Test Sections
                     * */
                    frisby
                        .post(config.endpoint + '/api/group/' + groupId + '/role/' + roleId)
                        .expect('status', 200)
                        .then(function () {
                            frisby
                                .get(config.endpoint + '/api/group/' + groupId + '/role/find')
                                .expect('status', 200)
                                .then(function (result) {
                                    var itemParam = result._body.items;
                                    var flag = false;
                                    itemParam.forEach(function (element) {
                                        if (element.id == roleId) {
                                            flag = true;
                                        }
                                    })
                                    expect(flag).toBe(true)

                                    /*
                                     * Delete Created Items
                                     * */
                                    frisby
                                        .del(config.endpoint + '/api/group/' + groupId)
                                        .expect('status', 200)
                                        .then(function () {
                                            frisby
                                                .del(config.endpoint + '/api/role/' + roleId)
                                                .expect('status', 200)
                                                .done(done);
                                        })
                                })
                        })
                })
        })
})

