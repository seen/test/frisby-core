/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
const frisby = require('frisby');
const Joi = frisby.Joi;
const config = require('../config.js');
const qs = require('qs');

//Do setup first
frisby.globalSetup({
	request: {
		headers: {
			'Authorization': 'Basic ' + Buffer.from(config.admin.login + ":" + config.admin.password).toString('base64'),
			'Content-Type': 'application/x-www-form-urlencoded'
		}
	}
});


it('Status 404 when group/id/user/id with bad user id', function (done) {
	var groupName = 'testGroup_' + config.randomString();
	/*
	 * Setup Test
	 */
	frisby
	.post(config.endpoint + '/api/group/new', {
		body: qs.stringify({
			'name': groupName,
			'description': 'when_CallAPIGroupIdUserIdGETwithBadUserId_Expect_Status404UserNotFound'
		})
	})
	.expect('successResponses')
	.then(function (res) {
		var groupId = res._body.id;

		/*
		 * Test Sections
		 * */
		frisby
		.get(config.endpoint + '/api/group/' + groupId + '/user/9999999')
		.expect('clientErrorResponses')
		/*
		 * Delete Created Items
		 * */
		.then(function () {
			frisby
			.del(config.endpoint + '/api/group/' + groupId)
			.expect('status', 200)
			.done(done);
		});
	});
});
