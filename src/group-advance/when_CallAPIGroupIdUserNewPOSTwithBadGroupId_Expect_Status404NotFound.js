/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
const frisby = require('frisby');
const Joi = frisby.Joi;
const config = require('../config.js');
const qs = require('qs');

//Do setup first
frisby.globalSetup({
	request: {
		headers: {
			'Authorization': 'Basic ' + Buffer.from(config.admin.login + ":" + config.admin.password).toString('base64'),
			'Content-Type': 'application/x-www-form-urlencoded'
		}
	}
});


it('Status 404 when POST group/id/user/new with bad Group id', function (done) {
	var userName = 'testUser_' + config.randomString();
	var password = 'testPass_' + config.passwordGenerator();

	/*
	 * Setup Test
	 * */
	frisby
	.post(config.endpoint + '/api/user/new', {
		body: qs.stringify({
			login: userName,
			first_name: "testTeam",
			last_name: "when_CallAPIGroupIdUserNewPOSTwithBadGroupId_Expect_Status404NotFound",
			password: password,
			email: config.randomNumber()+"@local.com"
		})
	})
	.expect('successResponses')
	.then(function (res) {
		var userId = res._body.id;

		/*
		 * Test Sections
		 * */
		frisby
		.post(config.endpoint + '/api/group/999999/user/new', {
			body: qs.stringify({
				user_id: userId,
			})
		})
		.expect('clientErrorResponses')
		/*
		 * Delete Created Items
		 * */
		.then(function () {
			frisby
			.del(config.endpoint + '/api/user/' + userId)
			.expect('successResponses')
			.done(done);
		});
	});
});
