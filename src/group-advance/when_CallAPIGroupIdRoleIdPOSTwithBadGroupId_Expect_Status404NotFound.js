/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
const frisby = require('frisby');
const Joi = frisby.Joi;
const config = require('../config.js');
const qs = require('qs');

//Do setup first
frisby.globalSetup({
	request: {
		headers: {
			'Authorization': 'Basic ' + Buffer.from(config.admin.login + ":" + config.admin.password).toString('base64'),
			'Content-Type': 'application/x-www-form-urlencoded'
		}
	}
});


it('Status 404 when POST group/id/role/id with bad Group id', function (done) {
	var roleName = 'testRole_' + config.randomString();

	/*
	 * Setup Test
	 * */
	frisby
	.post(config.endpoint + '/api/role/new', {
		body: qs.stringify({
			'name': roleName,
			'description': 'when_CallAPIGroupIdRoleIdPOSTwithBadGroupId_Expect_Status404NotFound',
			'application': 'testApp',
			'code_name': 'test_' + config.randomString()
		})
	})
	.expect('status', 200)
	.then(function (res) {
		var roleId = res._body.id;

		/*
		 * Test Sections
		 * */
		frisby
		.post(config.endpoint + '/api/group/999999/role/' + roleId)
		.expect('clientErrorResponses')
		/*
		 * Delete Created Items
		 * */
		.then(function () {
			frisby
			.del(config.endpoint + '/api/role/' + roleId)
			.expect('status', 200)
			.done(done);
		});
	});
});

