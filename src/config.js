/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
//var passGenerator = require('generate-password');
var uuid = require('uuid');
var randomInt = require('random-js')
exports.endpoint = 'http://localhost:8083';
exports.version = '1.0.0';
exports.sensorAddress = 'hadi-log.saberin.loc';

exports.admin = {
	login : 'admin',
	password : 'admin'
};

exports.fakeAdminPass = {
        login : 'admin1',
        password : 'admin'
};

exports.setCookie = function(headers) {
	var cookie = '';
	var setCookie = headers['set-cookie'];
	if (Array.isArray(setCookie)) {
		for (var i = 0, len = setCookie.length; i < len; i++) {
			cookie += setCookie[i].split(';')[0]
			if (i < len - 1)
				cookie += '; '
		}
	}
	return cookie;
};

exports.randomString = function () {
	return uuid();
};

/*
* Generate 10 char Random number
* */
exports.randomNumber = function () {
	var engine = randomInt.engines.nativeMath;
    return randomInt.integer(1111111111, 9999999999)(engine);
};

exports.passwordGenerator = function () {
    // var strictPassword = passGenerator.generate({
	// 	length: 8,
	// 	numbers: true,
     //    symbols: true,
     //    strict: true,
	// 	exclude: '!%^&*()+-=}{[]|:;/?><,`~"'
	// });
	return '1234@abcD';
};

if(process.env['endpoint']){
    exports.endpoint = process.env['endpoint'];
}

/*
 * Init frisby 
 * 
 */
const frisby = require('frisby');
beforeAll(function () {
	frisby.addExpectHandler('informationalResponses', function (response) {
		expect(response.status)
		.toBeGreaterThanOrEqual(100);
		expect(response.status)
		.toBeLessThan(200);
	});
	frisby.addExpectHandler('successResponses', function (response) {
		expect(response.status)
		.toBeGreaterThanOrEqual(200);
		expect(response.status)
		.toBeLessThan(300);
	});
	frisby.addExpectHandler('redirectionResponses', function (response) {
		expect(response.status)
		.toBeGreaterThanOrEqual(300);
		expect(response.status)
		.toBeLessThan(400);
	});
	frisby.addExpectHandler('clientErrorResponses', function (response) {
		expect(response.status)
		.toBeGreaterThanOrEqual(400);
		expect(response.status)
		.toBeLessThan(500);
	});
	frisby.addExpectHandler('serverErrorResponses', function (response) {
		expect(response.status)
		.toBeGreaterThanOrEqual(500);
		expect(response.status)
		.toBeLessThan(600);
	});
	
	frisby.addExpectHandler('paginatedListResponse', function (response) {
		// TODO: maso, 2018: check if body is page list
	});
});

