/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
const frisby = require('frisby');
const Joi = frisby.Joi;
const config = require('../config.js');
const qs = require('qs');

//Do setup first
frisby.globalSetup({
	request: {
		headers: {
			'Authorization': 'Basic ' + Buffer.from(config.admin.login + ":" + config.admin.password).toString('base64'),
			'Content-Type': 'application/x-www-form-urlencoded'
		}
	}
});


it('Create and Get it', function (done) {
	var keyName = 'testGroup_' + config.randomString();
	frisby
	/*
	 * Setup Test
	 * */
	.post(config.endpoint + '/api/saas/setting/new', {
		body: qs.stringify({
			'mode': 0,
			'description': 'when_CallAPISaasTagNewPOST_CallAPISassTagFind_Expect_TagIdContainResponse',
			'key': keyName,
			'value': 'test_' + config.randomString()
		})
	})
	.expect('status', 200)
	.then(function (res) {
		var settingId = res._body.id;
		/*
		 * Test Sections
		 * */
		frisby
		.get(config.endpoint + '/api/saas/setting/' + settingId)
		.expect('status', 200)
		.then(function (result) {
			expect(result._body.id).toBe(settingId);

			/*
			 * Delete Created Items
			 * */
			frisby
			.del(config.endpoint + '/api/saas/setting/' + settingId)
			.expect('status', 200)
			.done(done);
		});
	});
});

