/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
const frisby = require('frisby');
const Joi = frisby.Joi;
const config = require('../config.js');
const qs = require('qs');

//Do setup first
frisby.globalSetup({
	request: {headers: {'Content-Type': 'application/x-www-form-urlencoded',}}
});

it('Check Setting Mode ', function (done) {
	/*
	 * Setup Test
	 * */
	var privateKey = 'privateKey' + config.randomString();
	var publicKey = 'publicKey' + config.randomString();
	frisby

	/*
	 * Create Setting With Private Mode '1'
	 * */
	.post(config.endpoint + '/api/saas/setting/new', {
		body: qs.stringify({
			'mode': 1,
			'description': 'Check_Setting_Mode_Functionaliry_WithPrivateMode',
			'key': privateKey,
			'value': 'test_' + config.randomString()
		}),
		headers: {
			'Authorization': 'Basic ' + Buffer.from(config.admin.login + ":" + config.admin.password).toString('base64')
		}
	})
	.expect('status', 200)
	.then(function (res1) {
		var settingPrivateId = res1._body.id;

		/*
		 * Create Setting with Public Mode '0'
		 * */
		frisby
		.setup({
			request: {
				headers: {
					'Authorization': 'Basic ' + Buffer.from(config.admin.login + ":" + config.admin.password).toString('base64'),
				}
			}
		})
		.post(config.endpoint + '/api/saas/setting/new', {
			body: qs.stringify({
				'mode': 0,
				'description': 'Check_Setting_Mode_Functionaliry_WithPublicMode',
				'key': publicKey,
				'value': 'test_' + config.randomString()
			}),
			headers: {
				'Authorization': 'Basic ' + Buffer.from(config.admin.login + ":" + config.admin.password).toString('base64')
			}
		})

		.expect('status', 200)
		.then(function (res2) {
			var settingPublicId = res2._body.id;

			/*
			 * Test Sections
			 * */
			frisby

			/*
			 * Get Public Setting with Anonymous
			 * */
			.get(config.endpoint + '/api/saas/setting/' + publicKey)
			.expect('status', 200)
			// .expect('json', '*', {
			//     key: publicKey
			// })
			.then(function () {
				/*
				 * Get Private Setting with Anonymous
				 * */
				frisby
				.get(config.endpoint + '/api/saas/setting/' + privateKey)
				.expect('status', 404)//NotFound
				.then(function () {
					/*
					 * Get Private Setting with Authorized User
					 * */
					frisby
					.get(config.endpoint + '/api/saas/setting/' + privateKey, {
						headers: {
							'Authorization': 'Basic ' + Buffer.from(config.admin.login + ":" + config.admin.password).toString('base64')
						}
					})
					.expect('status', 200)
					// .expect('json', '*', {
					//     key: publicKey
					// })
					/*
					 * Delete Created Items
					 * */
					.then(function () {
						frisby
						.del(config.endpoint + '/api/saas/setting/' + settingPublicId, {
							headers: {
								'Authorization': 'Basic ' + Buffer.from(config.admin.login + ":" + config.admin.password).toString('base64')
							}
						})
						.expect('status', 200)
						.then(function () {
							frisby
							.del(config.endpoint + '/api/saas/setting/' + settingPrivateId, {
								headers: {
									'Authorization': 'Basic ' + Buffer.from(config.admin.login + ":" + config.admin.password).toString('base64')
								}
							})
							.expect('status', 200)
							.done(done);
						});
					});
				});
			});
		});
	});
});

