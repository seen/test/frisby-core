/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */

const frisby = require('frisby');
const fs = require('fs');
const format = require("string-template");
const qs = require('qs');


const admin = {
		login : 'admin',
		password : 'admin'
};

/******************************************************************************************************
 *                                     Util test
 * 
 * 
 ******************************************************************************************************/

/**
 * Load test config into the test
 */
function initTestWithConfig(test, testConfig) {
	// add expects
	if (testConfig.expects) {
		for (var i = 0; i < testConfig.expects.length; i++) {
			test = test.expect(testConfig.expects[i]);
			//console.log(testConfig.expects[i]);
		}
	}
	// add json type
	if (testConfig.jsonTypes) {
		if (testConfig.paginatedList) {
			test = test.expect('jsonTypes', 'items.*', testConfig.jsonTypes);
		} else {
			test = test.expect('jsonTypes', testConfig.jsonTypes);
		}
	}
	// add json
	if (testConfig.json && !testConfig.paginatedList) {
		test = test.expect('json', testConfig.json);
	}
	return test;
}


/******************************************************************************************************
 *                                     Binary test
 * 
 ******************************************************************************************************/


function createFormDataForModel(model, data) {
	var form = frisby.formData();
	switch (data.type) {
	case 'file': {
		form.append('file', fs.createReadStream(model.path), {
			knownLength : fs.statSync(model.path).size
		});
		break;
	}
	case 'text': {
		var buf = Buffer.from(data.content, 'utf8');
		form.append('file', buf, {
			filename : 'unicycle.txt', // ... or:
			filepath : 'unicycle.txt',
			contentType : 'text/plain',
			knownLength : buf.length
		});
		break;
	}
	case 'random': {
		var len = Math.random(100);
		form.append('file', new Buffer(len), {
			filename : 'unicycle.txt', // ... or:
			filepath : 'unicycle.txt',
			contentType : 'text/plain',
			knownLength : len
		});
		break;
	}
	}
	return form;
}
/** 
 * Post data with anonymous
 */
function createAnonymousPostBinaryTest(testConfig, model) {
	return function(done) {
		// TODO: maso, 2018: POST data
		var resourcePath = format(testConfig.resource, model);
//		console.log(resourcePath);
		var form = frisby.formData();
		form.append('file', new Buffer(10), {
			filename : 'unicycle.txt', // ... or:
			filepath : 'unicycle.txt',
			contentType : 'text/plain',
			knownLength : 10
		});
		// send a post request to create the image
		frisby // Post data
		.setup({
			request : {
				headers : Object.assign(testConfig.headers, {
					'content-type' : 'multipart/form-data; boundary=' + form.getBoundary(),
					'content-length' : form.getLengthSync()
				})
			}
		})
		.post(resourcePath, {
			body : form
		})
//		.inspectBody()
		.expect('clientErrorResponses')
		.done(done);
	};
}

/** 
 * Post data 
 */
function createPostBinaryTest(testConfig, model, data) {
	return function(done) {
		var form = createFormDataForModel(model, data);
		var test = frisby // Post data
		.setup({
			request : {
				headers : Object.assign(testConfig.headers, {
					'content-type' : 'multipart/form-data; boundary=' + form.getBoundary(),
					'content-length' : form.getLengthSync()
				})
			}
		})
		.post(format(testConfig.resource, model), {
			body : form
		});

		// init test
		initTestWithConfig(test, testConfig)
//		test
//		.inspectBody()
		.done(done);
//		console.log(format(testConfig.resource, model));
		// TODO: maso, 2018: download and compair content
	};
}

function basicProtectedBinaryTests(testConfig) {
	for (var i = 0; i < testConfig.models.length; i++) {
		// anonymous get list
		it('should fail with anonymous to upload for model : index=>' + i,
				createAnonymousPostBinaryTest(Object.assign({}, testConfig, {
					paginatedList : true,
					jsonTypes : false,
					headers : {},
					expects : [
						'clientErrorResponses'
						]
				}), testConfig.models[i]));
	}

	var testData = testConfig.testData || [];
	for (var j = 0; j < testData.length; j++) {
		for (var i = 0; i < testConfig.models.length; i++) {
			// anonymous get list
			it(format('should load data {0} for model {1}', i, j),
					createPostBinaryTest(Object.assign({}, testConfig, {
						paginatedList : true,
						jsonTypes : false,
						headers : {
							'Authorization' : 'Basic ' + Buffer.from(admin.login + ":" + admin.password).toString('base64'),
						},
						expects : [
							'successResponses',
							],
					}), testConfig.models[i], testData[j]));
		}
	}
}


module.exports.protectedBinaryTest = basicProtectedBinaryTests;


