/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */

const frisby = require('frisby');
const Joi = require('joi');
const schemasCore = require('./schemas-core.js');
/**
 * Init frisby 
 * 
 * @param frisby : instance of frisby test module
 */
function init(){
	frisby.addExpectHandler('informationalResponses', function(response) {
		expect(response.status)
		.toBeGreaterThanOrEqual(100);
		expect(response.status)
		.toBeLessThan(200);
	});
	frisby.addExpectHandler('successResponses', function(response) {
		expect(response.status)
		.toBeGreaterThanOrEqual(200);
		expect(response.status)
		.toBeLessThan(300);
	});
	frisby.addExpectHandler('redirectionResponses', function(response) {
		expect(response.status)
		.toBeGreaterThanOrEqual(300);
		expect(response.status)
		.toBeLessThan(400);
	});
	frisby.addExpectHandler('clientErrorResponses', function(response) {
		expect(response.status)
		.toBeGreaterThanOrEqual(400);
		expect(response.status)
		.toBeLessThan(500);
	});
	frisby.addExpectHandler('notFoundResponses', function(response) {
		expect(response.status)
		.toBe(404);
	});
	frisby.addExpectHandler('serverErrorResponses', function(response) {
		expect(response.status)
		.toBeGreaterThanOrEqual(500);
		expect(response.status)
		.toBeLessThan(600);
	});

	frisby.addExpectHandler('paginatedListResponse', function(response) {
		// maso, 2018: check if body is page list
		//		console.log(response.json);
		let result = Joi.validate(response.json, schemasCore.paginatedList, {
			allowUnknown : true
		});
		if (result.error) {
			throw result.error;
		}
	});
	frisby.addExpectHandler('emptyPaginatedListResponse', function(response) {
		// maso, 2018: check if body is page list
		let result = Joi.validate(response.json, schemasCore.paginatedList, {
			allowUnknown : true
		});
		// check page size
		expect(response.json.items.length)//
			.toBe(0);
		if (result.error) {
			throw result.error;
		}
	});

	/*
	 * Model contains an id>0
	 */
	frisby.addExpectHandler('modelItemResponse', function(response) {
		// maso, 2018: check if body is model
		let result = Joi.validate(response.json, schemasCore.model, {
			allowUnknown : true
		});
		if (result.error) {
			throw result.error;
		}
	});
}


// export moduel
module.exports.init = init;

