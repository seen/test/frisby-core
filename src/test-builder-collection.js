/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */

const frisby = require('frisby');
const fs = require('fs');
const format = require("string-template");
const qs = require('qs');


const admin = {
		login : 'admin',
		password : 'admin'
};

/******************************************************************************************************
 *                                     Util test
 * 
 * 
 ******************************************************************************************************/

/**
 * Load test config into the test
 */
function initTestWithConfig(test, testConfig) {
	// add expects
	if (testConfig.expects) {
		for (var i = 0; i < testConfig.expects.length; i++) {
			test = test.expect(testConfig.expects[i]);
			//console.log(testConfig.expects[i]);
		}
	}
	// add json type
	if (testConfig.jsonTypes) {
		if (testConfig.paginatedList) {
			test = test.expect('jsonTypes', 'items.*', testConfig.jsonTypes);
		} else {
			test = test.expect('jsonTypes', testConfig.jsonTypes);
		}
	}
	// add json
	if (testConfig.json && !testConfig.paginatedList) {
		test = test.expect('json', testConfig.json);
	}
	return test;
}

/**
 * Load test config into the test
 */
function initTestWithConfigForEmptyPage(test, testConfig) {
	// add expects
	if (testConfig.expects) {
		for (var i = 0; i < testConfig.expects.length; i++) {
			test = test.expect(testConfig.expects[i]);
			//console.log(testConfig.expects[i]);
		}
	}
	// add json
	if (testConfig.json && !testConfig.paginatedList) {
		test = test.expect('json', testConfig.json);
	}
	test
		.expect('successResponses')
		.expect('emptyPaginatedListResponse')
	return test;
}
/******************************************************************************************************
 *                                     Collection test
 * 
 * 
 ******************************************************************************************************/

/**
 * 
 * 
 * jsonTypes: schema of data
 * json: data (expected)
 * expects: array of expectation
 * headers: list of request header
 */
function createClearCollectionTest(testConfig, model) {
	return function(done) {
		// create test
		var setup = {
				request : {}
		};
		if (testConfig.headers) {
			setup.request.headers = testConfig.headers;
		}
		// Create test
		var test = frisby
			.setup(setup)
			.del(format(testConfig.resource, model));
//			.inspectBody()
		initTestWithConfigForEmptyPage(test, testConfig)
			.then(function(){
				frisby
					.setup(setup)
					.get(format(testConfig.resource, model))
//					.inspectBody()
					.expect('successResponses')
					.expect('emptyPaginatedListResponse')
					.done(done);
			});
	};
}

/**
 * 
 * 
 * jsonTypes: schema of data
 * json: data (expected)
 * expects: array of expectation
 * headers: list of request header
 */
function createGetCollectionTest(testConfig, model) {
	return function(done) {
		// create test
		var setup = {
				request : {}
		};
		if (testConfig.headers) {
			setup.request.headers = testConfig.headers;
		}
		// Create test
		var test = frisby
			.setup(setup)
			.get(format(testConfig.resource, model));
		initTestWithConfig(test, testConfig)
			.done(done);
	};
}


/**
 * Puts test data into the collecton
 */
function createPutCollectionTest(testConfig, data, model) {
	return function(done) {
		var path = format(testConfig.resource, model);
		// console.log(path);
		var test = frisby
		.post(path, {
			body : qs.stringify(data),
			headers : Object.assign(testConfig.headers, {
				'Content-Type' : 'application/x-www-form-urlencoded'
			})
		})
		.expect('json', data);
		// init test
		initTestWithConfig(test, testConfig)
		//.inspectBody()
		.done(done);
	};
}

/**
 * Puts test data into the collecton
 */
function createGetFromCollectionTest(testConfig, data, model) {
	return function(done) {
		var path = format(testConfig.resource, model);
		var test = frisby
		.post(path, {
			body : qs.stringify(data),
			headers : Object.assign(testConfig.headers, {
				'Content-Type' : 'application/x-www-form-urlencoded'
			})
		})
		.expect('json', data);
		// init test
		initTestWithConfig(test, testConfig)
//		.inspectBody()
		.then(function(res) {
			frisby
			.setup({
				request : {
					headers : Object.assign({}, testConfig.headers)
				}
			})
			.get(path + '/' + res.json.id)
//			.inspectBody()
			.expect('successResponses')
			.expect('jsonTypes', testConfig.jsonTypes || seen.schemas.core.model)
			.expect('json', data)
			.done(done);
		});
	};
}



/**
 * Creates basic collection tests
 * 
 * - Collection is protected agins anonymous access
 * - Admin can access the collection
 * 
 * TODO:
 * - Putting model into the collection is allowed
 * - Getting a model from collection is allowed
 * - Removeing a model from collection is allowed
 */
function basicProtectedCollectionTests(testConfig) {
	var testData = testConfig.testData || [];
	var models = testConfig.models || [ {} ];

	// anonymous get list
	for (var i = 0; i < models.length; i++) {
		it('should fail with anonymous', createGetCollectionTest(Object.assign({}, testConfig, {
			paginatedList : true,
			jsonTypes : false,
			expects : [
				'clientErrorResponses'
				]
		}), models[i]));
	}

	for (var i = 0; i < testData.length; i++) {
		for (var j = 0; j < models.length; j++) {
			// Put data into the collection
			it('Should add a new model:' + i, createPutCollectionTest(Object.assign({}, testConfig, {
				paginatedList : false,
				expects : [
					'successResponses'
					],
					headers : {
						'Authorization' : 'Basic ' + Buffer.from(admin.login + ":" + admin.password).toString('base64'),
					}
			}), testData[i], models[j]));
			// Put data into the collection
			it('Should get a new model by id:' + i, createGetFromCollectionTest(Object.assign({}, testConfig, {
				paginatedList : false,
				expects : [
					'successResponses'
					],
					headers : {
						'Authorization' : 'Basic ' + Buffer.from(admin.login + ":" + admin.password).toString('base64'),
					}
			}), testData[i], models[j]));
		}
	}

	// Admin get list
	for (var i = 0; i < models.length; i++) {
		it('should create a paginated list', createGetCollectionTest(Object.assign({}, testConfig, {
			paginatedList : true,
			json : false,
			expects : [
				'successResponses',
				'paginatedListResponse'
				],
				headers : {
					'Authorization' : 'Basic ' + Buffer.from(admin.login + ":" + admin.password).toString('base64'),
				}
		}), models[i]));
	}
}



/**
 * Creates basic collection tests
 * 
 * - Collection is protected agins anonymous access
 * - Admin can access the collection
 * 
 * TESTS:
 * - Remove multi item
 */
function bulkyProtectedCollectionTests(testConfig) {
	var testData = testConfig.testData || [];
	var models = testConfig.models || [ {} ];
	for (var j = 0; j < models.length; j++) {
		// clear collection
		it('Should clear an empty collection of model ' + j, createClearCollectionTest(Object.assign({}, testConfig, {
			paginatedList : true,
			expects : [
				'successResponses'
				],
				headers : {
					'Authorization' : 'Basic ' + Buffer.from(admin.login + ":" + admin.password).toString('base64'),
				}
		}), models[j]));
		for (var i = 0; i < testData.length; i++) {

			// Put data into the collection
			it('Should add a new model:' + i, createPutCollectionTest(Object.assign({}, testConfig, {
				paginatedList : false,
				expects : [
					'successResponses'
					],
					headers : {
						'Authorization' : 'Basic ' + Buffer.from(admin.login + ":" + admin.password).toString('base64'),
					}
			}), testData[i], models[j]));
		}
		// clear collection
		it('Should clear non-empty collection of model ' + j, createClearCollectionTest(Object.assign({}, testConfig, {
			paginatedList : true,
			expects : [
				'successResponses'
				],
				headers : {
					'Authorization' : 'Basic ' + Buffer.from(admin.login + ":" + admin.password).toString('base64'),
				}
		}), models[j]));
	}
}

////data
//exports.admin = admin;
//exports.fakeAdminPass = fakeAdminPass;

//functions
module.exports.createFetchTest = createGetCollectionTest;
module.exports.fetchProtectedTests = basicProtectedCollectionTests;
module.exports.bulkyProtectedTests = bulkyProtectedCollectionTests;

