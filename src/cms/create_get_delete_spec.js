/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
var frisby = require('frisby');
const Joi = frisby.Joi;
var config = require('../config.js');
const qs = require('qs');


//Do setup first
frisby.globalSetup({
	request: {
		headers: {
			'Authorization': 'Basic ' + Buffer.from(config.admin.login + ":" + config.admin.password).toString('base64'),
		}
	}
});


it('Must create a content and delete at the end', function(done){
	var prefixName = 'test-'+(Math.random()*1000);
	frisby
	.setup({
		request: {
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}
	})
	.post(config.endpoint + '/api/cms/new',{
		body: qs.stringify({
			'name': prefixName,
			'title': 'test content',
			'description': 'simple contetn to test',
			'mime_type': 'text/plain'
		})
	})
	.expect('status', 200)
	.done(done);
});

