/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
var frisby = require('frisby');
var config = require('../config.js');
var cookie = null;


function testDeleteMessage(message){
    frisby.create('delete a message' + message.id)//
    .addHeader('Cookie', cookie) //
    .delete(config.endpoint + '/api/message/'+message.id)//
    .expectStatus(200)//
    .toss();
}

function testGetMessage(message){
    frisby.create('get a message' + message.id)//
    .addHeader('Cookie', cookie) //
    .get(config.endpoint + '/api/message/'+message.id)//
    .expectStatus(200)//
    .after(function(err, res, body){
        testDeleteMessage(message);
    })//
    .toss();
}

function testFindMessage(){
    frisby.create('get messages')//
    .addHeader('Cookie', cookie) //
    .get(config.endpoint + '/api/message/find')//
    //.inspectBody()
    .expectStatus(200)//
    .after(function(err, res, body){
        var object = JSON.parse(body);
        object.items.forEach(function(message){
            testGetMessage(message);
        });
    })//
    .toss();//   
}
/*
 * List all roles
 */
frisby.create('Admin login')//
.post(config.endpoint + '/api/user/login', config.admin)//
.expectStatus(200)//
.after(function(err, res, body) {
    cookie = config.setCookie(res.headers);
    testFindMessage();
})//
.toss();
