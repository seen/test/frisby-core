/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
var frisby = require('frisby');
var config = require('../config.js');

var login = 'testUser' + Math.floor((Math.random() * 10000) + 1);
var password = 'pass' + Math.floor((Math.random() * 100) + 1);
var email = 'email-' + Math.floor((Math.random() * 10000) + 1) + '@dqp.co.ir';
var date = new Date("2015-03-25T12:00:00Z");
frisby.create('Create new account')//
.post(config.endpoint + '/api/user/new', {
	version : 0,
	login : login,
	first_name : "test",
	last_name : "test",
	password : password,
	email : email,
	administrator : 1,
	staff : 1,
	active : 1,
	language : "fa",
	timezone : "Asia/Tehran",
    date_joined : date,
    last_login : date

})//
.expectStatus(200)//
.expectJSONTypes({
	id : Number,
	login : String,
	active : Boolean,
	email : String
})//
.expectJSON({
	login : login,
	email : email
})//
.after(function(err, res, body) {
	var cookie = config.setCookie(res.headers);
	frisby.create('Login with the created account')//
	.addHeader('Cookie', cookie) //
	.post(config.endpoint + '/api/user/login', {
		login : login,
		password : password
	})//
	.expectJSONTypes({
		id : Number,
		login : String,
		active : Boolean
	})//
	.toss();//
})//
.toss();
