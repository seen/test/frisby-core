/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
var frisby = require('frisby');
var config = require('../config.js');

frisby.create('login with admin')
        .addHeader('Accept', 'application/json')//
.post(config.endpoint + '/api/user/login', config.admin)//
.expectStatus(200)//
.inspectBody()
//.expectJSONTypes({
//	id : Number,
//	login : String,
//	active : Boolean
//})//
.after(function(body, res) {
	var cookie = config.setCookie(res.headers);
	frisby.create('logout')//
	.addHeader('Cookie', cookie) //
        .addHeader('Accept', 'application/json')//
	.get(config.endpoint + '/api/user/logout')//
	.expectStatus(200)//
	.after(function(err, res, body) {
		cookie = config.setCookie(res.headers);
		frisby.create('get account info')//
		.addHeader('Cookie', cookie)// 
		.get(config.endpoint + '/api/user')//
		.after(function(err, res, body) {
			var user = JSON.parse(body);
			expect(user).not.toBe(null);
//			expect('id' in user).toBe(true);
		})//
		.toss();
	})//
	.toss();//
})//
.toss();
