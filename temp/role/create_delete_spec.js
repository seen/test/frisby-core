/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
var frisby = require('frisby');
var config = require('../config.js');

/*
 * List all roles
 */
frisby.create('Admin login')//
.post(config.endpoint + '/api/user/login', config.admin)//
.expectStatus(200)//
.after(function(err, res, body) {
	var cookie = config.setCookie(res.headers);
	// create
	for (var i = 0x0; i < 10; i++) {
		frisby.create('create a roles')//
		.addHeader('Cookie', cookie) //
		.post(config.endpoint + '/api/role/new',{
			  'version': '1',
			  'name': 'test application  permision',
			  'description': 'test',
			  'application': 'test',
			  'code_name': 'test'+i,
		})//
		.expectStatus(200)//
		.expectJSONTypes({
			id : Number,
		// TODO: maso, 1395: Add other attributes
		})//
		.after(function(err, res, body) {
			var role = JSON.parse(body);
			// remove
			frisby.create('delete a random roles:' + role.id)//
			.addHeader('Cookie', cookie) //
			.delete(config.endpoint + '/api/role/'+role.id)//
			.expectStatus(200)//
			.toss();//
		})//
		.toss();//
	}
})//
.toss();
