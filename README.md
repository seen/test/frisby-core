# frisby-core

Seen Core consists of the following systems:

- Tenant management system
- User management system
- Content management system

The test look for main functional of these modules.

# Requirement

Test in this project are runs by [Jest](https://jasmine.github.io/2.0/node.html) package.

Some other packages are needed by jest-frisby which are listed in the file package.json of project.

To install all dependencies run following command in root directory of project:

	npm install

# How to run

All Test cases should be in __tests__ folder to jest find and run them
General form of command to run test:

    jest [test suit]

    
# Pre-conditions

## Test data

There must be a user with the following cridentials:

- login: admin
- password: admin

The user must be able to do all things on the system.

## Authentication

System MUST support basic authentication.


